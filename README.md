# About React Native WebView Example

This repository is designed to showcase a minimalistic implementation of Spinwheel's Drop-In Modules. The app models how a third-party client could quickly be up and running using the Spinwheel functions: connecting a loan using the Connect DIM, displaying a customer's loan details with the Loan Pal DIM, and Navigating across different DIMs.

*Note*: This application was not originally intended to be a model, so it takes a number of liberties that are not in line with best practices like: not properly storing credentials in environmental variables, not using a proper router/controller/services pattern, not logging anything, and not making even the slightest attempt at handle errors gracefully. **As such, DO NOT, under any circumstance, directly cut and paste the code from this repository.**

# Getting Started
## Clone

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/react-native-dropin-example.git
```

Once cloned, follow below to get started:

This App is Build using the react native cli quicksart guide.

Install all the pre-requisites following: https://reactnative.dev/docs/environment-setup

### Note: Please make sure that the setup of Android/iOS emulator/simulator is done on the system by following the steps mentioned in above link.

## Steps to run after above setup:

1. Install dependencies

```
npm i
```

2. Navigate to backend/ folder and create a .env file
3. Add SECRET_KEY="<your-private-key>" in that file and save.
4. Install dependencies for backend

```
cd backend/ && npm i
```

5. Run backend

```
cd backend/ && npm start
```

6. In new terminal in the root folder

```
npx react-native start
```

7. Inside of "/src/constants" , change the ip of all the urls present to your current network ip.

*For Andriod*

8. Run below command

```
npx react-native run-android
```

*For iOS*

8. Run below command

```
cd ios && pod install
```

9. Run below command

```
npx react-native run-ios
```