import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useGetExtUserToken } from "../serviceRequests/service-requests";
import { Dimensions, Button, TouchableOpacity } from "react-native";
import StudentLoanScreen from "./StudentLoansScreen";


export default function Home() {
    const [token, setToken] = useState("");

    const fetchToken = async () => {
        const data = await useGetExtUserToken('randomuser-123456');
        console.log('token in fetched', token, data)
        setToken(data);
    };

    return (
        token ? (
            <StudentLoanScreen token={token}/>
                ) : (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={fetchToken} > 
                    <Text>Connect to Loan</Text>
                </TouchableOpacity>
            </View>
        )
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get("window").width ,
      marginTop: 45,
      height: Dimensions.get("window").height ,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#00B2E3",
        padding: 10,
        margin: 35
    }
});