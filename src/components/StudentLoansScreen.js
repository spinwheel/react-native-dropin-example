import React, { useEffect } from "react";
import { StyleSheet, Text, Platform } from "react-native";
import AutoHeightWebView from "react-native-autoheight-webview";
import { Dimensions } from "react-native";
import { AppConstants } from "../constants";
import useSWHandlers from "../spinWheelEventHandler";

export default function StudentLoanScreen({token}) {
  const webviewRef = React.useRef();
  const embedUrl = AppConstants.WEBVIEW_URL

  const Js = useSWHandlers(token, Platform.OS);


  const handleOnMessage = (eventPayload) => {
    let dataPayload;
    try {
      dataPayload = JSON.parse(eventPayload.nativeEvent.data);

    } catch (e) {}
    console.log("Window events", dataPayload, 'token in message', token)
    if (dataPayload.loadedPage) {
        webviewRef.current.injectJavaScript(Js);
        webviewRef.current.postMessage(JSON.stringify({'dropInToLoad':'connect'}));
    }
    if(dataPayload.eventName == 'AUTH_SUCCESS') {
      webviewRef.current.postMessage(JSON.stringify({'dropInToLoad':'loanpal'}));
    }
    if (dataPayload.type === 'Console') {
      console.info(`[Console] ${JSON.stringify(dataPayload.data)}`);
    }
  }
 
  return (
    token? (
      <AutoHeightWebView
        ref={webviewRef}
        style={styles.container}
        originWhitelist={["*"]}
        source={{ uri: embedUrl} }
        scalesPageToFit={true}
        viewportContent={'width=device-width, user-scalable=no'}
        onMessage={handleOnMessage}
    />
    ) : (
      <Text style={styles.container}>Loading..</Text>
    )
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: Dimensions.get("window").width ,
      marginTop: 45,
      height: Dimensions.get("window").height ,
    }
});