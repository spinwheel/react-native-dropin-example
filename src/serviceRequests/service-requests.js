import { AppConstants } from "../constants";
import axios from "axios";

export async function useGetExtUserToken(extUserId ) {
    const resp = await axios({
        method: "get",
        url: `${AppConstants.BASE_URL + AppConstants.endpoints.getToken}/${extUserId}`,
    });
    return resp.data;
}
