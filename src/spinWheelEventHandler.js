export default function useSWHandlers(token, platform) {
    console.log('token in handler', token, 'platform',platform)
    return `
    const consoleLog = (type, log) => window.ReactNativeWebView.postMessage(JSON.stringify({'type': 'Console', 'data': {'type': type, 'log': log}}));
    console = {
        log: (log) => consoleLog('log', log),
        debug: (log) => consoleLog('debug', log),
        info: (log) => consoleLog('info', log),
        warn: (log) => consoleLog('warn', log),
        error: (log) => consoleLog('error', log),
    };
    console.log("inside handler")

    const token = "${token}"
    const  createSpinwheelhandler = (config)  => Spinwheel.create(config);
    const createConfig = (config= {}) =>{
        return {
            containerId: "dropin-container",
            env: "sandbox",
            
            onSuccess: (metadata) => {
                metadata.testprop = 'hello'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onLoad: (metadata) => {
                metadata.testprop = 'hello1'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onExit: (metadata) => {
                metadata.testprop = 'hello2'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onSettled: (metadata) => {
                metadata.testprop = 'hello3'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onEvent: (metadata) => {
                metadata.testprop = 'hello4'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onError: (metadata) => {
                metadata.testprop = 'hello5'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            onResize: (metadata) => {
                metadata.testprop = 'hello6'
                window.ReactNativeWebView.postMessage(JSON.stringify(metadata));
            },
            ...config
        }
    }
    const openConnectDIM=(token)=> {
        const newConfig = createConfig( {
            dropinConfig: {
            module: "loan-servicers-login",
            token: token,
            header: 0
        }})
        const handler = createSpinwheelhandler(newConfig)
        try {
            handler.open();
        } catch (err) {
            console.log('ERROR',err);
        }
    }
    const openLoanPalDIM=(token)=> {
        const newConfig = createConfig( {
            dropinConfig: {
            module: "loan-pal",
            token: token,
            header: 0
        }})
        const handler = createSpinwheelhandler(newConfig)
        try{
            handler.open();
        }catch (err) {
            console.log('ERROR',err);
        }
    }
   
    const handleMessage = (event) => {
        let eventData = JSON.parse(event.data);
        switch (eventData.dropInToLoad) {
            case 'connect': {
            openConnectDIM(token)
            break;
            }
            case 'loanpal': {
                openLoanPalDIM(token)
            break;
            }
            default:
            dropinContentData = null;
        }
    }
    if("${platform}" == 'ios') {
        window.addEventListener('message', handleMessage)
    } else if("${platform}" == 'android') {
        document.addEventListener('message', handleMessage)
    }
`
}
// document.querySelector('.content-header-buttons daily-header-buttons').style.display='none'; 